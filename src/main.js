// Make sure auth.env exists with the configuration below. See auth.env.example
const twilio = require('twilio');
// const accountSid = "AC1...."; // Your Account SID from https://www.twilio.com/console
const accountSid = process.env.AUTH_SID;
// const authToken = "abc123...."; // Your Account SID from https://www.twilio.com/console
const authToken = process.env.AUTH_TOKEN;
// phoneTo: '+18009876543',
const phoneTo = process.env.PHONE_TO;
// phoneFrom: '+18009876543',  // https://www.twilio.com/console/phone-numbers/incoming
const phoneFrom = process.env.PHONE_FROM;
const callerId = process.env.CALLER_ID;
const client = new twilio(accountSid, authToken);

function sms() {
  return client.messages.create({
    body: 'Hello from Node twilio-test',
    to: phoneTo,
    from: phoneFrom,
  })
  .then((message) => console.log("sms", message.sid), console.error);
}

function voice() {
  client.calls.create({
    // url: 'http://demo.twilio.com/docs/voice.xml',
    url: 'https://erosson.gitlab.io/twilio-test/message.xml',
    to: phoneTo,
    from: phoneFrom,
    callerId: callerId,
  })
  .then(call => console.log("voice", call.sid), console.error)
  .done();
}
// Uncomment your favorite line below.

// send one sms
// sms();

// send one voice call
// voice();

// send multiple sms, one every 10 sec
// setInterval(sms, 10 * 1000);

// send multiple voice calls, one every 100 sec
// setInterval(voice, 100 * 1000);
